<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StartrickTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StartrickTable Test Case
 */
class StartrickTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\StartrickTable
     */
    public $Startrick;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Startrick'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Startrick') ? [] : ['className' => StartrickTable::class];
        $this->Startrick = TableRegistry::getTableLocator()->get('Startrick', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Startrick);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
