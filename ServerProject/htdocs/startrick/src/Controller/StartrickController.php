<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Startrick Controller
 *
 * @property \App\Model\Table\StartrickTable $Startrick
 *
 * @method \App\Model\Entity\Startrick[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StartrickController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $startrick = $this->paginate($this->Startrick);

        $this->set(compact('startrick'));
    }

    /**
     * View method
     *
     * @param string|null $id Startrick id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $startrick = $this->Startrick->get($id, [
            'contain' => []
        ]);

        $this->set('startrick', $startrick);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $startrick = $this->Startrick->newEntity();
        if ($this->request->is('post')) {
            $startrick = $this->Startrick->patchEntity($startrick, $this->request->getData());
            if ($this->Startrick->save($startrick)) {
                $this->Flash->success(__('The startrick has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The startrick could not be saved. Please, try again.'));
        }
        $this->set(compact('startrick'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Startrick id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $startrick = $this->Startrick->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $startrick = $this->Startrick->patchEntity($startrick, $this->request->getData());
            if ($this->Startrick->save($startrick)) {
                $this->Flash->success(__('The startrick has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The startrick could not be saved. Please, try again.'));
        }
        $this->set(compact('startrick'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Startrick id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $startrick = $this->Startrick->get($id);
        if ($this->Startrick->delete($startrick)) {
            $this->Flash->success(__('The startrick has been deleted.'));
        } else {
            $this->Flash->error(__('The startrick could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function getResult()
    {
        error_log("getResult()");
        $this->autoRender = false;
        $query = $this->Startrick->find('all');

        $query->order(['Score'=>'DESC']);//DESC

        $json_array =json_encode($query);
        echo $json_array;
    }

    public function setRanking(){
		error_log("setRankinge()");
		$this->autoRender = false;
		// name,message をPOSTで受け取る。
		$name		= "";
		if( isset( $this->request->data['name'] ) ){
			$name	= $this->request->data['name'];
			error_log($name);
		}
        $score	= "";
        if( isset( $this->request->data['score'] ) )
        {
			$score	= $this->request->data['score'];
			error_log($score);
        }
        $cleartime	= "";
        if( isset( $this->request->data['cleartime'] ) )
        {
			$cleartime	= $this->request->data['cleartime'];
			error_log($cleartime);
        }

        $data	= array ( 'Name' => $name,'Score' => $score,'Cleartime' => $cleartime, 'Date' => date('Y/m/d H:i:s'));
        $startrick = $this->Startrick->newEntity();
		$startrick = $this->Startrick->patchEntity($startrick, $data);
        if ($this->Startrick->save($startrick)) {
			//追加成功
            echo "1";
		}else{
            //失敗
            error_log(json_encode($this->Startrick->getDataSource()->getLog()));
            echo "0";
            //echo $startrick;
            //echo $startrick->getErrors();
        }
	}
}