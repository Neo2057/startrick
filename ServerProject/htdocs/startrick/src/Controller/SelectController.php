<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Select Controller
 *
 *
 * @method \App\Model\Entity\Select[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SelectController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $select = $this->paginate($this->Select);

        $this->set(compact('select'));
    }

    /**
     * View method
     *
     * @param string|null $id Select id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $select = $this->Select->get($id, [
            'contain' => []
        ]);

        $this->set('select', $select);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $select = $this->Select->newEntity();
        if ($this->request->is('post')) {
            $select = $this->Select->patchEntity($select, $this->request->getData());
            if ($this->Select->save($select)) {
                $this->Flash->success(__('The select has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The select could not be saved. Please, try again.'));
        }
        $this->set(compact('select'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Select id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $select = $this->Select->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $select = $this->Select->patchEntity($select, $this->request->getData());
            if ($this->Select->save($select)) {
                $this->Flash->success(__('The select has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The select could not be saved. Please, try again.'));
        }
        $this->set(compact('select'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Select id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $select = $this->Select->get($id);
        if ($this->Select->delete($select)) {
            $this->Flash->success(__('The select has been deleted.'));
        } else {
            $this->Flash->error(__('The select could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
