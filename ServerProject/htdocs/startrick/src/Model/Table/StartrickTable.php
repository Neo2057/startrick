<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Startrick Model
 *
 * @method \App\Model\Entity\Startrick get($primaryKey, $options = [])
 * @method \App\Model\Entity\Startrick newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Startrick[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Startrick|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Startrick saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Startrick patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Startrick[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Startrick findOrCreate($search, callable $callback = null, $options = [])
 */
class StartrickTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('startrick');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Id')
            ->allowEmptyString('Id', null, 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 15)
            ->requirePresence('Name', 'create')
            ->notEmptyString('Name');

        $validator
            ->integer('Cleartime')
            ->requirePresence('Cleartime', 'create')
            ->notEmptyTime('Cleartime');

        $validator
            ->integer('Score')
            ->requirePresence('Score', 'create')
            ->notEmptyString('Score');

        $validator
            ->dateTime('Date')
            ->notEmptyDateTime('Date');

        return $validator;
    }
}
