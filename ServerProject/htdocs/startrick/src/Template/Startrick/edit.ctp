<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Startrick $startrick
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $startrick->Id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $startrick->Id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Startrick'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="startrick form large-9 medium-8 columns content">
    <?= $this->Form->create($startrick) ?>
    <fieldset>
        <legend><?= __('Edit Startrick') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('Cleartime');
            echo $this->Form->control('Score');
            echo $this->Form->control('Date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
