<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Startrick $startrick
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Startrick'), ['action' => 'edit', $startrick->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Startrick'), ['action' => 'delete', $startrick->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $startrick->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Startrick'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Startrick'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="startrick view large-9 medium-8 columns content">
    <h3><?= h($startrick->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($startrick->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($startrick->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Score') ?></th>
            <td><?= $this->Number->format($startrick->Score) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cleartime') ?></th>
            <td><?= h($startrick->Cleartime) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($startrick->Date) ?></td>
        </tr>
    </table>
</div>
