-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.6-MariaDB
-- PHP のバージョン: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `techtest`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `startrick`
--

CREATE TABLE `startrick` (
  `Id` int(11) NOT NULL,
  `Name` varchar(15) NOT NULL,
  `Cleartime` int(255) NOT NULL,
  `Score` int(255) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `startrick`
--

INSERT INTO `startrick` (`Id`, `Name`, `Cleartime`, `Score`, `Date`) VALUES
(13, 'I♡', 1, 1600, '2019-10-08 05:54:48'),
(14, 'Jon_Mikel', 0, 2700, '2019-10-08 06:02:49'),
(15, 'May', 0, -2457, '2019-10-09 22:00:07'),
(16, 'プリン', 0, 8992, '2019-10-09 22:04:43'),
(17, 'プリン', 0, -608, '2019-10-09 22:20:21'),
(18, 'チーター', 1, 99999, '2019-10-10 07:38:19'),
(19, 'ねお', 0, 19414, '2019-10-09 22:49:24');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `startrick`
--
ALTER TABLE `startrick`
  ADD PRIMARY KEY (`Id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `startrick`
--
ALTER TABLE `startrick`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
