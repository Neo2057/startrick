﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSctipt : MonoBehaviour
{
    public GameObject targetObject1;
    public GameObject targetObject2;

    public GameObject bullet;
    public Transform startgun;

    public float rotatemode = 1.0f;
    float gunspeed = 10000f;
    public float rotatespeed = 0.07f;

    public float span = 3;
    private float currentTime = 0f;

    void Start()
    {
        
    }
    void Update()
    {
        Vector3 TargetPos1 = targetObject1.transform.position;
        Vector3 TargetPos2 = targetObject2.transform.position;
        float dis1 = Vector3.Distance(this.transform.position, TargetPos1);
        float dis2 = Vector3.Distance(this.transform.position, TargetPos2);

        currentTime += Time.deltaTime;

        if (dis1 < 700 && dis1 < dis2)
        {
            //砲台
            Vector3 relativePos1 = targetObject1.transform.position - transform.GetChild(1).transform.position;
            Quaternion rotation1 = Quaternion.LookRotation(relativePos1);
            transform.GetChild(1).rotation = Quaternion.Slerp(transform.GetChild(1).transform.rotation, rotation1, rotatespeed);
            //向き

            if (currentTime > span)
            {
                GameObject bullets = Instantiate(bullet, startgun.position, transform.GetChild(1).rotation * Quaternion.Euler(90, 0, 0)) as GameObject;

                Vector3 force;

                force = transform.GetChild(1).gameObject.transform.forward * gunspeed;

                bullets.GetComponent<Rigidbody>().AddForce(force);
                currentTime = 0f;
            }
        }
        else if(dis2 < 700 && dis2 < dis1)
        {            //砲台
            Vector3 relativePos2 = targetObject2.transform.position - transform.GetChild(1).transform.position;
            Quaternion rotation2 = Quaternion.LookRotation(relativePos2);
            transform.GetChild(1).rotation = Quaternion.Slerp(transform.GetChild(1).transform.rotation, rotation2, rotatespeed);
            //向き

            if (currentTime > span)
            {
                GameObject bullets = Instantiate(bullet, startgun.position, transform.GetChild(1).rotation * Quaternion.Euler(90, 0, 0)) as GameObject;

                Vector3 force;

                force = transform.GetChild(1).gameObject.transform.forward * gunspeed;

                bullets.GetComponent<Rigidbody>().AddForce(force);
                currentTime = 0f;
            }
        }
    }

    void LateUpdate()
    {
        //　カメラと同じ向きに設定
        //transform.rotation = Camera.main.transform.rotation;
    }
}
