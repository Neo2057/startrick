﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainGameManager: MonoBehaviour
{
    public Text ScoreText;
    public Text NameText;
    public Text TimerText;

    public float totaltime;
    int seconds;
    public GameObject player;
    public GameObject playerShip;
    public GameObject Pudding;
    public Camera startcamera;
    
    void Update()
    {
        ScoreText.text = "Score:"+SaveData.Instance.score;
        NameText.text = SaveData.Instance.name;

        totaltime -= Time.deltaTime;
        seconds = (int)totaltime;
        if(1 > seconds)
        {
            TimerText.text = "";
            OnStart();
            startcamera.enabled = false;
            player.SetActive(true);
            if(SaveData.Instance.name != "プリン")
            {
                playerShip.SetActive(true);
                Pudding.SetActive(false);
            }else
            {
                playerShip.SetActive(false);
                Pudding.SetActive(true);
            }
        }
        else
        {
            TimerText.text = seconds.ToString();
        }
    }
    void OnStart()
    {

    }
}
