﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoalScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
     
    }

    string DiffenceShip;
    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "DiffenceShip")
        {
            Debug.Log(collision.gameObject.name);
            SceneManager.LoadScene("ResultScene");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        SceneManager.LoadScene("ResultScene");
    }
}
