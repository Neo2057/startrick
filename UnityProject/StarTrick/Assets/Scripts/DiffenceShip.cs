﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DiffenceShip : MonoBehaviour
{
    public int maxHP = 2500;
    //現在のHP
    public float currentHP = 2500;

    public GameObject textObj;
    public Text text;
    public Animator anima;



    public Image image;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        textObj.GetComponent<Text>().text = ((int)currentHP).ToString();
        image.GetComponent<Image>().fillAmount = currentHP / maxHP;

        if (0 >= currentHP)
        {
            SaveData.Instance.score -= 5000;
            Object.Destroy(this.gameObject);
            SceneManager.LoadScene("ResultScene");
            float slowmove = currentHP / maxHP;
            anima.GetComponent<Animator>().SetFloat("Speed", 1.6f);
        }
    }

    string Goal;
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Goal")
        {
            SaveData.Instance.clear = true;
            SaveData.Instance.score += (int)currentHP;
            SceneManager.LoadScene("ResultScene");
        }
    }


    string TurretAmmo;
    private void OnTriggerEnter(Collider collsion)
    {
        //Debug.Log(collsion.gameObject.name);
        if (0 <= currentHP && collsion.gameObject.name == "TurretAmmo(Clone)")
        {
            currentHP -= 1;
        }
        if (0 <= currentHP && collsion.gameObject.name == "Bomb")
        {
            currentHP -= 50;
        }
        if (0 <= currentHP && collsion.gameObject.name == "care")
        {
            currentHP += 30;
        }
    }
}
