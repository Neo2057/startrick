﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using UnityEngine.Networking;

public class MainResultManager : MonoBehaviour
{
    [SerializeField] private Text _displayField = default;
    private List<RankingData> _rankingList;

    public Text text;
    public Text ScoreText;
    public Text NameText;
    public Text ClearText;
    public Text ClearSubText;
    public Text CleartarText;
    public Button setButton;

     void Start()
    {
        if(SaveData.Instance.clear == true)
        {
            ClearText.text = "STAGE CLEAR";
            ClearSubText.text = "congratulations!";
            CleartarText.text = "STAGE CLEAR";
        }
        else
        {
            ClearText.text = "GAME OVER";
            ClearSubText.text = "";
            CleartarText.text = "GAME OVER";
        }

        ScoreText.text = SaveData.Instance.name+"のスコアは"+SaveData.Instance.score;

        getjsonwww();
    }

    private void Update()
    {
        
    }

    private void getjsonwww()
    {
        _displayField.text = " \n \n \n \n \n \n データベースに接続中...";

        //WebRequest
        StartCoroutine(
            DownloadJson(
                RequestSuccess,
                RequestFailed
                )
            );
    }

    private void RequestSuccess(string response){
        _rankingList = RankingDataModel.DeserializeFromJson(response);

        _displayField.text = "OK";}
    private void RequestFailed(){
        _displayField.text = "jsonデータの取得に失敗しました！";}



    private IEnumerator DownloadJson(Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        UnityWebRequest www = UnityWebRequest.Get("http://localhost/startrick/startrick/getresult");
        yield return www.SendWebRequest();
        if (www.error !=null)
        {
            Debug.LogError(www.error);
            if (null != cbkFailed)
            {
                
            }

            {
                cbkFailed();
            }
        }
        else if (www.isDone)
        {
            Debug.Log($"Success:{www.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
                getjson();//受け取った物を映す
            }
        }
    }

    private void CallbackWwwFailed()
    {
        // jsonデータ取得に失敗した
        _displayField.text = "Www Failed";
    }

    private void CallbackApiSuccess(string response)
    {
        // json データ取得が成功したのでデシリアライズして整形し画面に表示する
        _displayField.text = response;
    }

    private void getjson()
    {
        string sStrOutput = "";
        int rank = 0;

        if (null == _rankingList)
        {
            sStrOutput = " \n \n \n \n \n リストが見つからないか接続中です";
        }
        else
        {
            sStrOutput += $"名前-|-スコア\n";
            foreach (RankingData memberOne in _rankingList)
            {
                rank += 1;
                sStrOutput +=
                    $"{rank} 位 " +
                    $"{memberOne.Name} : " +
                    $"{memberOne.Score} \n";
                //$"{memberOne.Id}位 : " +
                //$"{memberOne.Cleartime} : " +
                //$"Date:{memberOne.Date} \n";
            }
        }
        _displayField.text = sStrOutput;
    }
    public void clicksetrank()
    {
        _displayField.text = "接続中...";
        SetJsonFromWww();
    }

    private void SetJsonFromWww()
    {
        string sTgtURL = "http://localhost/startrick/startrick/setRanking";

        string name = SaveData.Instance.name;
        string score = SaveData.Instance.score.ToString();
        string cleartime = SaveData.Instance.cleartime.ToString();
        //int cleartime = SaveData.Instance.cleartime;

        StartCoroutine(SetRanking(sTgtURL, name, score, cleartime, CallbackApiSuccess, CallbackWwwFailed));
        setButton.interactable = false;
    }
    private IEnumerator SetRanking(string url,string name,string score,string cleartime,Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm from = new WWWForm();
        from.AddField("name", name);
        from.AddField("score", score);
        from.AddField("cleartime", cleartime);

        var webRequest = UnityWebRequest.Post(url, from);

        webRequest.timeout = 10;

        yield return webRequest.SendWebRequest();

        if (webRequest.error != null)
        {
            Debug.LogError(webRequest.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            Debug.Log($"Success:{webRequest.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
        getjsonwww();
    }



    public void OnClickMenuBackButton(){
        SceneManager.LoadScene("MenuScene");
    }
}
