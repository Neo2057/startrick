﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Playercontrol : MonoBehaviour
{
    public Camera FCamera;
    public Camera SCamera;
    public Camera TCamera;

    private Rigidbody rb;
    private Transform player;
    public float wspeed;
    public float rotatespeed;

    public GameObject pbarret;
    public Transform startpos;

    public float span = 0;
    private float currentTime = 2f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        player = GameObject.FindGameObjectWithTag("Player").transform;
        if (player == null)
        {
            player = transform;
        }
        FCamera.enabled = false;
        SCamera.enabled = false;
        TCamera.enabled = true;
    }
    
    void Update()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");

        if (Input.GetKeyDown(KeyCode.Q))
        {
            Debug.Log("Push Q key!");
            if (FCamera.enabled == true)
            {
                FCamera.enabled = false;
                SCamera.enabled = true;
            }
            else if (SCamera.enabled == true)
            {
                SCamera.enabled = false;
                TCamera.enabled = true;
            }
            else if (TCamera.enabled == true)
            {
                TCamera.enabled = false;
                FCamera.enabled = true;
            }
        }
        currentTime += Time.deltaTime;
        if (currentTime > span)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                GameObject PlayerBullet = Instantiate(pbarret, startpos.position, startpos.rotation * Quaternion.Euler(90,0,0));
                Vector3 force;
                float gunspeed = 30000f;
                force = player.transform.forward * gunspeed;
                PlayerBullet.GetComponent<Rigidbody>().AddForce(force);
            }
            currentTime = 0f;
        }
    }


    void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.W)) {
            transform.Translate(Vector3.forward * wspeed);
        }
        if (Input.GetKey(KeyCode.A)) {
            // 左方向に回転させる
            transform.Rotate(Vector3.down * rotatespeed);
        }
        if (Input.GetKey(KeyCode.D)) {
            transform.Rotate(Vector3.up * rotatespeed);
        }
        if ((Input.GetKey(KeyCode.LeftShift)) && (Input.GetKey(KeyCode.W)))
        {
            rb.transform.Rotate((-1) * rotatespeed, 0, 0);
        }
        if ((Input.GetKey(KeyCode.LeftShift)) && (Input.GetKey(KeyCode.S)))
        {
            rb.transform.Rotate(rotatespeed, 0, 0);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back * wspeed);
        }
    }
    string Goal;
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.name == "Goal")
        {
            SceneManager.LoadScene("ResultScene");
        }
        if (collision.gameObject.name == "DiffenceShip")
        {
            this.rb.constraints = RigidbodyConstraints.FreezePosition;
            this.rb.constraints = RigidbodyConstraints.FreezeRotation;
            this.rb.constraints = RigidbodyConstraints.None;
            this.rb.constraints = RigidbodyConstraints.FreezeRotation;
        }
    }
}