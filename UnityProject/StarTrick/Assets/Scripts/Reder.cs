﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reder : MonoBehaviour
{
    public GameObject PlayerRotate;
    public GameObject RaderPlayer;

    void Start()
    {
        
    }


    // Update is called once per frame
    void Update()
    { 
        float _Rotation = PlayerRotate.transform.localEulerAngles.y;
        RaderPlayer.transform.rotation = Quaternion.Euler(0f, 0f,(-1) * _Rotation);
    }
}
