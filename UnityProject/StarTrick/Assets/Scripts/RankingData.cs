﻿public class RankingData
{
    public string Name { get; set; }
    public string Cleartime { get; set; }
    public string Score { get; set; }
    public string Date { get; set; }

    public RankingData()
    {
        Name = "";
        Cleartime = "";
        Score = "";
        Date = "";
    }
}
