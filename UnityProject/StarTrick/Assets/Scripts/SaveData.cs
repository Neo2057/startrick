﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveData : MonoBehaviour
{
    public readonly static SaveData Instance = new SaveData();
    public bool pudding = false;
    public int score = 0;
    public new string name = "";
    public int cleartime = 0000;
    public bool clear = false;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
