﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretHpSystem : MonoBehaviour
{

    [SerializeField]
    int maxHP = 700;
    //現在のHP
    [SerializeField]
    float currentHP;
    //[SerializeField]
    //private GameObject destroyEffect;
    [SerializeField]
    int score = 100;
    public Slider slider;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        slider.GetComponent<Slider>().value = currentHP / maxHP;
        //currentHP -= Time.time * 1;

        if (0 >= currentHP)
        {
            SaveData.Instance.score += score;
            //GameObject effect = Instantiate(destroyEffect,this.transform);
            Object.Destroy(this.gameObject);
        }
    }
    
    private void OnTriggerEnter(Collider collsion)
    {
        if (0 <= currentHP && collsion.gameObject.name == "PlayerAmmo(Clone)")
        {
            currentHP -= 1;
        }
    }
}
