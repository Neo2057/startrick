﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HPDamageSystem : MonoBehaviour
{
    [SerializeField]
    int maxHP = 700;
    //現在のHP
    [SerializeField]
    float currentHP;

    public GameObject textObj;
    public Text text;
    public GameObject hpSystem;

    void Start(){
    }

    void Update()
    {
        //TextのTextコンポーネントにアクセス
        //(int)はfloatを整数で表示するためのもの
        textObj.GetComponent<Text>().text = ((int)currentHP).ToString();

        //HPSystemのスクリプトのHPDown関数に2つの数値を送る
        hpSystem.GetComponent<HPSystem>().HPDown(currentHP, maxHP);

        if (0 >= currentHP)
        {
            SaveData.Instance.score -= 3000;
            SceneManager.LoadScene("ResultScene");
        }
    }

    void FixedUpdate()
    {
    }

    string TurretAmmo;
    private void OnTriggerEnter(Collider collsion)
    {
        //Debug.Log(collsion.gameObject.name);
        if (0 <= currentHP && collsion.gameObject.name == "TurretAmmo(Clone)" )
        {
            currentHP -= 1;
        }
        if (0 <= currentHP && collsion.gameObject.name == "Bomb")
        {
            currentHP -= 50;
        }
        if (0 <= currentHP && collsion.gameObject.name == "care")
        {
            currentHP += 30;
        }
    }
}
