﻿using System.Collections;
using System.Collections.Generic;
using MiniJSON;
using System;

public class RankingDataModel
{
    public static List<RankingData> DeserializeFromJson(string sStrJson){
        var ret = new List<RankingData>();

        IList jsonList = (IList)Json.Deserialize(sStrJson);


        foreach (IDictionary jsonOne in jsonList)
        {
            //新レコード解析開始
            var tmp = new RankingData(); 
            //該当するキー名が jsonOne に存在するか調べ、存在したら取得して変数に格納する
            if (jsonOne.Contains("Name"))
            {
                tmp.Name = (string)jsonOne["Name"];
            }
            if (jsonOne.Contains("Cleartime"))
            {
                tmp.Cleartime = jsonOne["Cleartime"].ToString();
            }
            if (jsonOne.Contains("Score"))
            {
                tmp.Score = jsonOne["Score"].ToString();
            }
            
            ret.Add(tmp);
        }
        return ret;
    }
}
